\babel@toc {spanish}{}
\contentsline {section}{\numberline {1}Introducción}{3}{section.1}%
\contentsline {subsection}{\numberline {1.1}Agradecimientos}{3}{subsection.1.1}%
\contentsline {subsection}{\numberline {1.2}Resumen}{4}{subsection.1.2}%
\contentsline {subsection}{\numberline {1.3}Objetivos}{5}{subsection.1.3}%
\contentsline {subsection}{\numberline {1.4}Motivación}{6}{subsection.1.4}%
\contentsline {section}{\numberline {2}Estado del arte}{7}{section.2}%
\contentsline {subsection}{\numberline {2.1}¿Qué es un ANPR?}{7}{subsection.2.1}%
\contentsline {subsection}{\numberline {2.2}Historia de los sistemas de ANPR}{9}{subsection.2.2}%
\contentsline {subsection}{\numberline {2.3}¿Dónde se aplica actualmente un ANPR?}{11}{subsection.2.3}%
\contentsline {section}{\numberline {3}Desempeño del trabajo}{14}{section.3}%
\contentsline {subsection}{\numberline {3.1}Tecnologías utilizadas}{14}{subsection.3.1}%
\contentsline {subsection}{\numberline {3.2}Estudio de los datos necesarios}{17}{subsection.3.2}%
\contentsline {subsection}{\numberline {3.3}Procesamiento de la imagen}{20}{subsection.3.3}%
\contentsline {subsection}{\numberline {3.4}Tratamiento de los datos}{28}{subsection.3.4}%
\contentsline {subsection}{\numberline {3.5}Obtención de los resultados finales}{31}{subsection.3.5}%
\contentsline {section}{\numberline {4}Análisis de rendimiento}{33}{section.4}%
\contentsline {section}{\numberline {5}Conclusiones y planes de mejora}{37}{section.5}%
\contentsline {section}{\numberline {6}Bibliografía}{38}{section.6}%
\contentsline {section}{\numberline {7}Anexos y código fuente}{39}{section.7}%
