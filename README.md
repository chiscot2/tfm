# TFM

# Este es el repositorio dedicado al TFM: Detección y lectura de matrículas en imágenes

# Autor: Francisco Pais Fondo

# Tutor: Miguel Ángel Gómez López

Para ejecutar el programa ejecutaremos una sentencia en el terminal de la siguiente forma: python3 numberPlate.py "nombreImagen"

El directorio "Imagenes" almacena todos los archivos de imagen utilizados para validar el algoritmo.

Le memoria se encuentra disponible en el directorio "Memoria". Será un PDF que se generará a través del sistema de composición de texto Latex y el editor TexStudio.
