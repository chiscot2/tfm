import numpy as np
import cv2
import sys
from copy import deepcopy
from PIL import Image
import pytesseract


def cargadoDeImagen(img): #En este método se realiza un primer tratamiento de la imagen. Para ello reducimos el ruido que pueda contener.
	imgBlurred = cv2.GaussianBlur(img, (5,5), 0) #Aplicamos un suavizado Gaussiano.
	gray = cv2.cvtColor(imgBlurred, cv2.COLOR_BGR2GRAY) #Cambiamos el color de la imagen a blanco y negro.
	bil = cv2.bilateralFilter(gray, 11, 17, 17) #Aplicamos un filtro bilateral para reducir el ruido.
	ret2,threshold_img = cv2.threshold(bil,150,255,cv2.THRESH_BINARY) #Realizamos una segmentación de la imagen a través de los grados de intensidad de cada píxel
	return threshold_img #Devolvemos la imagen procesada

def extraccionDeRectangulos(threshold_img):
	element = cv2.getStructuringElement(shape=cv2.MORPH_RECT, ksize=(17, 3)) #Obtenemos una estructura con las medidas aproximadas de una matrícula que utilizaremos como patrón para validar posibles candidatos.
	morph_img_threshold = threshold_img.copy() #Realizamos una copia de la imagen.
	morph_img_threshold = cv2.morphologyEx(src=threshold_img, op=cv2.MORPH_CLOSE, kernel=element, dst=morph_img_threshold) #Realizamos operaciones de expansión y corrosión sobre la imagen original.
	contours, hierarchy= cv2.findContours(morph_img_threshold,mode=cv2.RETR_EXTERNAL,method=cv2.CHAIN_APPROX_NONE) #Obtenemos los posibles contornos en la imagen.
	return contours

def comprobacionDeColor(plate): #Método que comrpueba la gama de colores de la matrícula.
	avg = np.mean(plate) #Obtenemos una media del tono de los colores del trozo de la imagen procesada.
	if(avg>=90): #Si el tono es mayor de 90 aceptamos la comprobación.
		return True
	else:
 		return False

def comprobarRatioMatricula(rect):
	(x, y), (width, height), rect_angle = rect  #Obtenemos el ancho y el alto de la imagen.
	if (width!=0 and height!=0): #Comprobamos que la imagen tiene ancho y alto
		if ((height/width>3.5 and height/width<8 and height>155) or (width/height>3.5 and width/height<8 and width>155)): #Comprobamos el ancho y el alto para ver si cumplen el ratio de decisión establecido.
			return True
		return False
	return False

def validacionDeCaracteres(img,contours): #Validamos los caracteres existentes en la matrícula.
	for i,cnt in enumerate(contours): #Iteramos sobre los contornos de la imagen.
		min_rect = cv2.minAreaRect(cnt) #Obtenemos el mínimo area del contorno.
		validate = comprobarRatioMatricula(min_rect) #Comprobamos el ratio de la matrícula.
		if validate: #Si cumple la validación del ratio continuamos.
			x,y,w,h = cv2.boundingRect(cnt) #Dibujamos un rectángulo aproximado al rededor del contorno.
			plate_img = img[y:y+h,x:x+w] #Obtenemos la parte de la imagen que finalmente vamos a procesar.
			color = comprobacionDeColor(plate_img) #Comprobamos el color.
			if color: #Si cumple la validación del ratio continuamos.
				plate_im = Image.fromarray(plate_img) #Transformamos la imagen en un array de caracteres.
				text = pytesseract.image_to_string(plate_im, config='--psm 7 -c tessedit_char_whitelist=0123456789ABCDEFGHIJKLMNÑOPQRSTUVWXYZ-') #Intentamos adivinar el texto a partir de la imagen. Indicamos los caracteres disponibles en la imagen.
				print("La matricula es: ",text) #Pintamos el resultado final
				cv2.destroyAllWindows() #Destruimos todas las posibles ventanas que se hayan originado en el procesado.
				break

if __name__ == '__main__':
	print ("Se inicia la detección de matrícula...")
	img = cv2.imread("./Imagenes/"+str(sys.argv[1])) #Cargamos la imagen.
	threshold_img = cargadoDeImagen(img) #Realizamos un primer procesado de la imagen.
	contours = extraccionDeRectangulos(threshold_img) #Obtenemos los contornos existentes en la imagen.
	validacionDeCaracteres(threshold_img,contours) #Validamos los contornos para ver si alguno de ellos contiene los caracteres buscados.
	print ("Finaliza la detección de matrícula...")
